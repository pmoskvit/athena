#
# File specifying the location of Starlight to use.
#

set( STARLIGHT_LCGVERSION r300 )
set( STARLIGHT_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/starlight/${STARLIGHT_LCGVERSION}/${LCG_PLATFORM} )
